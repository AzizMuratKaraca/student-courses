export interface Student{
    id: number;
    name: string;
    age: string;
    course: string;
    studentCode: string;
}